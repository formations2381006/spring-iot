package pt.rumos.springiot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringIotApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringIotApplication.class, args);
	}

}
