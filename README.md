# Spring IoT

## Getting started
* author: __Eduardo Rocha__


## Deploy

### local run

__linux__   
    
    mvmw spring-boot:run

__windows__

    mvnw.cmd spring-boot:run

> NOTE: `JAVA_HOME` must be declared on user variables

```sh
$ export | grep JAVA_HOME
declare -x  JAVA_HOME="/usr/lib/jvm/
java-17-openjdk-amd64"
```

### local build

__clear target build folder__

    mvnw.cmd clean

__taget build__

    mvnw.cmd package

## RUN

    java -jar target/<app>.jar

## Activate profiles

### With enviroment variables

    set spring_profiles_active=<profile>

### From maven ops

    mvnw.cmd spring-boot:run -Dspring-boot.run.profiles=development

### From java ops

    java -jar target/<app>.jar --spring.profiles.acive=production


    mvnw.cmd test
